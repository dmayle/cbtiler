var data = JSON.parse(localStorage.getItem("data") || "{}");

/**
 * Communicate with external script
 */
chrome.runtime.onMessageExternal.addListener(
  function (request, sender, sendResponse) {
    var msg = JSON.parse(request);

    switch (msg.type) {
      case "add":
        sendResponse(data);
        break;
      case "get":
        sendResponse(data);
        break
      default:
        break;
    }
  }
);

// Full height: 1280x2104
// Half height: 1280x1052
// Two-thirds: 2560x2104
// One-half: 1920x2104
chrome.commands.onCommand.addListener(function(command) {
  if (command === "undock-tab") {
    chrome.windows.getLastFocused({"windowTypes": ["normal", "popup", "panel", "app", "devtools"], "populate": true}, function(window) {
      for (var index = 0; index < window.tabs.length; ++index) {
        if (window.tabs[index].highlighted) {
          chrome.windows.create({"tabId": window.tabs[index].id, "focused": true});
          return;
        }
      }
    });
    return;
  } else if (command === "dock-tab") {
    chrome.windows.getLastFocused({"windowTypes": ["normal", "popup", "panel", "app", "devtools"], "populate": true}, function(sourceWindow) {
      if (sourceWindow === undefined) {
        console.log('No suitable window found');
        return;
      }
      if (sourceWindow.tabs.length > 1) {
        console.log('Tab is already docket');
        return;
      }
      var matchingWindow = null;
      var maxOverlapPercentage = 0;
      chrome.windows.getAll({"windowTypes": ["normal", "popup", "panel", "app", "devtools"]}, function(windows) {
        for (var index = 0; index < windows.length; ++index) {
          var targetWindow = windows[index];
          if (targetWindow.id == sourceWindow.id) {
            continue;
          }
          // Check for overlap
          if ((targetWindow.left > (sourceWindow.left + sourceWindow.width)) ||
              (targetWindow.top > (sourceWindow.top + sourceWindow.height)) ||
              (sourceWindow.left > (targetWindow.left + targetWindow.width)) ||
              (sourceWindow.top > (targetWindow.top + targetWindow.height))) {
            // No overlap
            continue;
          }
          // Overlap is defined as the ratio between the overlapping region and the smaller of the two windows.
          var smallerWindow = null;
          if (sourceWindow.width * sourceWindow.height < targetWindow.width * targetWindow.height) {
            smallerWindow = sourceWindow;
          } else {
            smallerWindow = targetWindow;
          }
          var leftWindow = null;
          var rightWindow = null;
          var topWindow = null;
          var bottomWindow = null;
          if (sourceWindow.left < targetWindow.left) {
            leftWindow = sourceWindow;
            rightWindow = targetWindow;
          } else {
            leftWindow = targetWindow;
            rightWindow = sourceWindow;
          }
          if (sourceWindow.top < targetWindow.top) {
            topWindow = sourceWindow;
            bottomWindow = targetWindow;
          } else {
            topWindow = targetWindow;
            bottomWindow = sourceWindow;
          }
          var overlapArea = (leftWindow.left + leftWindow.width - rightWindow.left) * (topWindow.top + topWindow.height - bottomWindow.top);
          var overlapPercentage = overlapArea / (smallerWindow.width * smallerWindow.height);
          if (overlapPercentage > maxOverlapPercentage) {
            maxOverlapPercentage = overlapPercentage;
            matchingWindow = targetWindow;
          }
        }
        if (matchingWindow === null) {
          return;
        }
        console.log('Moving tab');
        chrome.tabs.move(sourceWindow.tabs[0].id, {"windowId": matchingWindow.id, "index": -1});
        chrome.tabs.update(sourceWindow.tabs[0].id, {"highlighted": true, "active": true});
        chrome.windows.update(matchingWindow.id, {"focused": true});
      });
    });
    return;
  }
  chrome.system.display.getInfo({
      "singleUnified": true  // If screens are mirrored, return one screen not two
    }, function(displays) {
      // Temp cheat and assume all screens the same
      var display = displays[0];
      var dim = display.workArea;
      var width = dim.width;
      var thirdWidth = Math.floor(dim.width/3);
      var twoThirdsWidth = Math.floor((2*dim.width)/3);
      var halfWidth = Math.floor(dim.width/2);
      var height = dim.height;
      var halfHeight = Math.floor(dim.height/2);
      var thirdHeight = Math.floor(dim.height/3);
      var twoThirdsHeight = Math.floor((2*dim.height)/3);
      chrome.windows.getLastFocused({"windowTypes": ["normal", "popup", "panel", "app", "devtools"]}, function(window) {
        console.log("Called for window: ", window);
        var screenOffset = window.left >= width ? width : 0;
        // Move it
        if (command ==="position-a") {
          // Move focused window to position a
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-b") {
          // Move focused window to position b
          chrome.windows.update(window.id, {"top": halfHeight, "left": screenOffset, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-c") {
          // Move focused window to position c
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + thirdWidth, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-d") {
          // Move focused window to position d
          chrome.windows.update(window.id, {"top": halfHeight, "left": screenOffset + thirdWidth, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-e") {
          // Move focused window to position e
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + twoThirdsWidth, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-f") {
          // Move focused window to position f
          chrome.windows.update(window.id, {"top": halfHeight, "left": screenOffset + twoThirdsWidth, "width": thirdWidth, "height": halfHeight, state: "normal"});
        } else if (command ==="position-1") {
          // Move focused window to position 1
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": thirdWidth, "height": height, state: "normal"});
        } else if (command ==="position-2") {
          // Move focused window to position 2
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + thirdWidth, "width": thirdWidth, "height": height, state: "normal"});
        } else if (command ==="position-3") {
          // Move focused window to position 3
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + twoThirdsWidth, "width": thirdWidth, "height": height, state: "normal"});
        } else if (command ==="position-4") {
          // Move focused window to position 4
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": twoThirdsWidth, "height": height, state: "normal"});
        } else if (command ==="position-5") {
          // Move focused window to position 5
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + thirdWidth, "width": twoThirdsWidth, "height": height, state: "normal"});
        } else if (command ==="position-6") {
          // Move focused window to position 6
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": halfWidth, "height": height, state: "normal"});
        } else if (command ==="position-7") {
          // Move focused window to position 7
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset + halfWidth, "width": halfWidth, "height": height, state: "normal"});
        } else if (command ==="position-x") {
          // Move focused window to position x
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": thirdWidth, "height": thirdHeight, state: "normal"});
        } else if (command ==="position-y") {
          // Move focused window to position y
          chrome.windows.update(window.id, {"top": thirdHeight, "left": screenOffset, "width": thirdWidth, "height": thirdHeight, state: "normal"});
        } else if (command ==="position-z") {
          // Move focused window to position z
          chrome.windows.update(window.id, {"top": twoThirdsHeight, "left": screenOffset, "width": thirdWidth, "height": thirdHeight, state: "normal"});
        } else if (command ==="position-m") {
          // Move focused window to position m
          chrome.windows.update(window.id, {"top": 0, "left": screenOffset, "width": width, "height": height, state: "normal"});
        } else if (command ==="move-right" || command ==="move-left") {
          // Since I only have two screens, and I want wrapping, this is the same
          chrome.windows.update(window.id, {"left": window.left >= width ? window.left - width : window.left + width, state: "normal"});
        }
      });
  });
});

// chrome.runtime.onInstalled.addListener(function(object) {
//   chrome.storage.local.get('installed', function(a) {
//     chrome.tabs.create({
//       url: chrome.runtime.getURL("pages/releaseNotes.html")
//     }, function (tab) {});
//   });
// });
